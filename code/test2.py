import os
import warnings

import numpy as np
import pandas as pd
#import matplotlib
#matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import sklearn
from sklearn import svm
from sklearn.model_selection import train_test_split
# from sdv.tabular import CTGAN
from ctgan import CTGANSynthesizer
import pickle
# import autosklearn.classification
# test_np = np.loadtxt('data\\california_smokers_cohort_csc_2011\\data.csv', delimiter='\t') # Does not work for
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from mods.helpers import *
from sklearn.experimental import enable_halving_search_cv
from sklearn.model_selection import HalvingGridSearchCV
### SMOTE ###
from imblearn.over_sampling import SMOTE



def rest():
    test = pd.read_csv('data/california_smokers_cohort_csc_2011/data.csv') # Load CSV using pandas
    who = pd.read_csv('data/who/RGTE11_dataset/2010_RGTE11.csv', encoding="ISO-8859-1")
    # Drop from table
    drop_column = test.drop(['RIGHTSEX'], axis=1)
    ## Preprocessing ##
    ## Preprocessing For encoding string labels as numbers in data
    #Le = LabelEncoder()
    #Le.fit_transform(Xtobeencoded)

    X= None
    y =None
    X_train,X_test, y_train, y_test = train_test_split(X,y, test_size=0.2)
    who_eur = who[who.REGION == "EUR"]
    row1 = who_eur.iloc[0]
    #who_eur.Popn_2010.plot() # PLot column Popn_2010 for all within region EUR
    #plt.show()
    #who_eur.plot(x=who_eur.COUNTRY,y="Popn_2010") # PLot column Popn_2010 for all within region EUR
    ##plt.show()

    ## Classification Tasks Models ##
    # K-nearest neighbors
    knn = sklearn.neighbors.KNeighborsClassifier(n_neighbors=25, weights='uniform')
    svc = sklearn.svm.SVC
    # knn.fit(X=X_train,y=y_train)
    # prediciton = knn.predict(x)

    #Evaluation
    #accuracy = metrics.accruacy_score(y_test, prediction)
    print("Done")


# def readmittion_smote():
    # from imblearn.over_sampling import SMOTE
    #data = pd.read_csv('data\\kaagle_hops_readmission\\train.csv', encoding="ISO-8859-1")
    #y = data.readmitted  # Get labels
    #base_features = [c for c in data.columns if c != "readmitted"]  # Extract all other features
    #X = data[base_features]  # Collect features for all samples in x
    #plot_x = pca_dim_reduction(X).as_matrix()

    #fig = plt.figure()
    #ax1 = fig.add_subplot(121)
    #ax1.scatter(plot_x[:, 0],  plot_x[:, 1], marker='.', c=y,
    #            s=25, edgecolor='k')
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    ## SMOTE ##
    ## random_state=42 controls randomization
    ## sampling_strategy should be set to 'all' instead of 'auto'-> minority
    #sm = SMOTE(sampling_strategy='all')  # Initialize SMOTE
    # X_res, y_res = sm.fit_resample(X_train, y_train) # Fit and resample using SMOTE

    # plot_aug_x = pca_dim_reduction(X_res).as_matrix()
    # ax2 = fig.add_subplot(122)
    # ax2.scatter(plot_aug_x[:, 0], plot_aug_x[:, 1], marker='.', c=y_res,
     #            s=25, edgecolor='k')
    # plt.show()
    # rnd_forest_orig = sklearn.ensemble.RandomForestClassifier(n_estimators=50)
    # rnd_forest_orig.fit(X_train,y_train)
    # y_pred_orig = rnd_forest_orig.predict(X_test)
    # print("Score on original data: {}".format(sklearn.metrics.accuracy_score(y_test,y_pred_orig)))

    # rnd_forest_aug = sklearn.ensemble.RandomForestClassifier(n_estimators=50)
    # rnd_forest_aug.fit(X_res, y_res)
    # y_pred_aug = rnd_forest_aug.predict(X_test)
    # print("Score on augmented data: {}".format(sklearn.metrics.accuracy_score(y_test, y_pred_aug)))

    #return None


def smoteing(df_data,idx_cat_cols=[1,3,4,5], ytarget_label='charges'):
    ## SMOTE ##
    random_state = 42  # controls randomization
    ## sampling_strategy should be set to 'all' to resample all data, instead of 'auto'-> minority
    #sm = SMOTE(sampling_strategy='all', random_state=random_state)  # Initialize SMOTE
    # smotenc = SMOTENC([1], random_state=101)  # [1] categorical features at index xyz...
    smotenc = SMOTENC(idx_cat_cols, random_state=random_state, sampling_strategy='all')
    X_train, y_train = split(df_data, pkey=ytarget_label)
    X_aug, y_aug = smotenc.fit_resample(X_train, y_train)  # Fit and resample using SMOTE
    return X_aug, y_aug


def loop_datasets():
    data1 = pd.read_csv('data/kaagle_hops_readmission/train.csv', encoding="ISO-8859-1")
    fit_ctgan(data1,'ctgan_1_ka_readd_e1000_b100_g256_d256.pkl')
    print("Done fitting 1")
    data2 = pd.read_csv('data/kaagle_mirichoi2018/insurance.csv', encoding="ISO-8859-1")
    fit_ctgan(data2, 'ctgan_1_ka_mirichoi2018_e1000_b100_g256_d256.pkl')
    print("Done fitting 2")
    data2 = pd.read_csv('data/freiburg_sets/pseudonymisierter_einzeldatensatz.csv', encoding="ISO-8859-1")
    fit_ctgan(data2, 'ctgan_1_freiburg1_arbeitlos_e1000_b100_g256_d256.pkl')


def main():
    data = pd.read_csv('data/kaagle_hops_readmission/train.csv', encoding="ISO-8859-1")
    y = data.readmitted  # Get labels
    base_features = [c for c in data.columns if c != "readmitted"]  # Extract all other features
    X = data[base_features]  # Collect features for all samples in x
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    aug_ctgan = get_ctgan(data)
    X_aug_ctgan = aug_ctgan[base_features]
    y_aug_ctgan = aug_ctgan.readmitted
    X_aug_ctgan_train, X_aug_ctgan_test, y_aug_ctgan_train, y_aug_ctgan_test = train_test_split(X_aug_ctgan,
                                                                                                y_aug_ctgan,
                                                                                                test_size=0.2)
    plot_aug_ctgan_x = pca_dim_reduction(X_aug_ctgan).to_numpy()
    fig = plt.figure()
    ax2 = fig.add_subplot(122)
    ax2.scatter(plot_aug_ctgan_x[:, 0], plot_aug_ctgan_x[:, 1], marker='.', c=y_aug_ctgan,
                s=25, edgecolor='k')
    plt.show()
    # readmittion_smote()
    print("Done")


def readdmission_ctgan(model_path='models/ctgan_1_ka_readd_e1000_b100_g256_d256.pkl',n_samples=1000):
    model = CTGAN.load(model_path)
    samples = model.sample(n_samples)
    y = samples.readmitted  # Get labels
    base_features = [c for c in samples.columns if c != "readmitted"]  # Extract all other features
    X = samples[base_features]  # Collect features for all samples in x
    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    return (X, y)
    # pca_reduced_x = pca_dim_reduction(X).to_numpy()
    # plot_pca(pca_reduced_x,y)


def fit_rnd_forest(X_train, X_test, y_train, y_test):
    rnd_forest_orig = sklearn.ensemble.RandomForestClassifier(n_estimators=50)
    rnd_forest_orig.fit(X_train, y_train)
    y_pred_orig = rnd_forest_orig.predict(X_test)
    print("Score on data: {}".format(sklearn.metrics.accuracy_score(y_test, y_pred_orig)))


def main_readd():
    test_size = 0.2
    # readmitted_data = get_dataset('data/kaagle_hops_readmission/train.csv', yLabel='readmitted')
    # df_data = get_dataset('data/kaagle_hops_readmission/train.csv')
    # fit_ctgan(df_data, 'models/readd_ctgan__wth_target_e1000_b200.pkl', epochs=1000, bsize=200)
    # fit_tvae(df_data, 'models/readd_tvae_i_wth_target_e1000_b500.pkl', epochs=1000, bsize=500)
    ## ctgan = load_ctgan('models/readd_ctgan__wth_target_e1000_b200.pkl')
    ## ctgan_samples = ctgan.sample(2000)
    ## ctgan_samples_prep = do_pipeline(ctgan_samples)
    ## ctgan_X, ctgan_y = split(ctgan_samples, 'readmitted')
    ##ct_mm_gs_res = gridsearch_classification(ctgan_X, ctgan_y)
    ## tvae = TVAE.load('models/readd_tvae_i_wth_target_e1000_b500.pkl')
    ## tvae_samples = tvae.sample(2000)
    ## tvae_samples_prep = do_pipeline(tvae_samples)
    ## tvae_X, tvae_y = split(tvae_samples, 'readmitted')
    ## t_mm_gs_res = gridsearch_classification(tvae_X, tvae_y)
    do_evaluations_readd()
    df_data_xy = get_dataset('data/kaagle_hops_readmission/train.csv', yLabel='readmitted')
    mm_gs_res = gridsearch_classification(df_data_xy[0], df_data_xy[1])
    mm_gs_res.score_summary().to_csv('readdmission_real_score_summary.csv')
    X_train, X_test, y_train, y_test = train_test_split(df_data_xy[0], df_data_xy[1], test_size=test_size)
    smote_aug_x, smote_aug_y = fit_smote((X_train, y_train))
    mm_gs_res_smote = gridsearch_classification(smote_aug_x, smote_aug_y)
    mm_gs_res_smote.score_summary().to_csv('readdmission_smote_score_summary_def.csv')
    # fit_rnd_forest(X_train, X_test, y_train, y_test)
    #Do CTGAN Sampling
    # ctgan_aug_data = readdmission_ctgan(n_samples=2000)
    ## X_train, X_test, y_train, y_test = train_test_split(ctgan_aug_data[0], ctgan_aug_data[1], test_size=test_size)
    ## fit_rnd_forest(X_train, X_test, y_train, y_test)
    print("Returning from Readdmission")


def get_coefficients(model):
    coefs = pd.DataFrame(
        model.named_steps['transformedtargetregressor'].regressor_.coef_,
        columns=['Coefficients'], index=feature_names
    )


def gridsearch(X,y ,model, parameters={'kernel': ('linear', 'poly', 'rbf', 'sigmoid'), 'C': [1, 2]}):
    clf = HalvingGridSearchCV(model, parameters,verbose=1, n_jobs=4) # e.g. SVM.SVC as model
    fitted_models = clf.fit(X, y)
    gs_res = pd.DataFrame(clf.cv_results_)
    #sorted(clf.cv_results_.keys())
    return fitted_models, gs_res


def main_synthea():
    test_size = 0.2
    data = get_dataset('data/synthea/csv/patients.csv')
    # X_train, X_test, y_train, y_test = train_test_split(data[0], data[1], test_size=test_size)
    #Do CTGAN Sampling
    # pearson(data)
    ctg_labels_synthea = ['PREFIX', 'SUFFIX', 'MAIDEN', 'MARITAL', 'RACE', 'ETHNICITY', 'GENDER', 'BIRTHPLACE',
                          'ADDRESS', 'CITY', 'STATE', 'COUNTY']
    relevant_features = pearson_target(data, target_key='HEALTHCARE_EXPENSES')
    print(relevant_features)
    data_enc = one_hot_encode_categorical(data, col_labels=ctg_labels_synthea)
    relevant_features = pearson_target(data_enc, target_key='HEALTHCARE_EXPENSES')
    print("ENCODED\n{}".format(relevant_features))
    # fit_ctgan(data, 'synthea1_def_constructor_1000iters.pkl', 'Id')
    aug_data = get_ctgan(name='models/synthea1_def_constructor_1000iters.pkl')
    X, y = choose_as_label(aug_data, 'HEALTHCARE_EXPENSES')
    # X.BIRTHDATE = pd.to_datetime(X.BIRTHDATE) #.dt.total_seconds().astype(int)
    # X = X[X['BIRTHDATE'].notnull()].copy()
    X = X.fillna(0)  # Fill NaN values with zero...
    X['BIRTHDATE'] = pd.to_datetime(X.BIRTHDATE).astype('int64')
    X['DEATHDATE'] = pd.to_datetime(X.DEATHDATE).astype('int64')
    # X = X.to_numpy()
    X = remove_columns(X, col_labels=['SSN', 'FIRST', 'LAST', 'DRIVERS', 'PASSPORT'])
    X, codes = make_categorical(X, col_labels=ctg_labels_synthea)
    #pca_dim_reduction(X.to_numpy())
    #X = preprocess(X)

    # y = preprocess(y)
    # X.GENDER, codes = pd.factorize(X.GENDER)
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
    #y = y.to_numpy()
    ## GRID SEARCH ##
    #regr = svm.SVR() # Test Model
    # X.plot()
    # pca_reduced_x = pca_dim_reduction(X, n_comp=2).to_numpy()
    # plot_pca_c(pca_reduced_x, y.to_numpy())
    # pearson(X)
    #plot_pca_r(pca_reduced_x,y) #For n_comp=2
    #mm_gs_res = mm_gridsearch(X.to_numpy(), y.to_numpy(), r_models1, r_params1)
    mm_gs_res = mm_gridsearch(X.to_numpy(), y.to_numpy(), r_models1, r_params1)
    #fitted_models, gs_res = gridsearch(X_train, y_train, regr)
    #plot_grid_search(gs_res, gs_res.param_C, gs_res.param_kernel, 'C', 'kernel')
    #plot_config(gs_res, param_1=None)

    print("DONE GRID SEARCHING")
    print("Breakp")
    # fit_rnd_forest(X_train, X_test, y_train, y_test)   # used for categorical estimations
    reg = LinearRegression().fit(X_train, y_train)
    res = reg.score(X, y)
    print("Linear regression: {}".format(res))
    #regr = make_pipeline(StandardScaler(), SVR(C=1.0, epsilon=0.2))
    regr = svm.SVR()
    regr.fit(X_train, y_train)
    svr_score = regr.score(X_test, y_test)
    print("SVR: {}".format(svr_score))
    print("Done")
    #ctgan_aug_data = readdmission_ctgan(n_samples=10000)
    #X_train, X_test, y_train, y_test = train_test_split(ctgan_aug_data[0], ctgan_aug_data[1], test_size=test_size)
    #fit_rnd_forest(X_train, X_test, y_train, y_test)

def fit_ctgan_miro(df_data):
    # pearson(data)
    categorical_labels = ['sex', 'smoker', 'region']
    data_encoded = one_hot_encode_categorical(df_data, categorical_labels)
    pearson(data_encoded)
    relevant_features = pearson_target(data_encoded, target_key='charges', threshold=0.5)
    print(relevant_features)
    del relevant_features['smoker_no']  # remove correlated feature to smoker_yes
    filtered_data = data_encoded[relevant_features.keys()]
    # filtered_data[] = data_encoded[relevant_features.keys()]
    filtered_data['smoker_yes'] = filtered_data['smoker_yes'].astype('bool')
    fit_ctgan_advanced(filtered_data, 'models/mirichoi_std_1', field_transformers={})


def mirichoi_train(df_data):
    # pearson(data)
    categorical_labels = ['sex', 'smoker', 'region']
    data_encoded = one_hot_encode_categorical(df_data, categorical_labels)
    pearson(data_encoded)
    relevant_features = pearson_target(data_encoded, target_key='charges', threshold=0.5)
    print(relevant_features)
    del relevant_features['smoker_no']  # remove correlated feature to smoker_yes
    filtered_data = data_encoded[relevant_features.keys()]
    # filtered_data[] = data_encoded[relevant_features.keys()]
    filtered_data['smoker_yes'] = filtered_data['smoker_yes'].astype('bool')
    # fit_ctgan_advanced(filtered_data, 'models/mirichoi_std_1', field_transformers={})
    print(data_encoded[["age", "bmi"]].corr())
    print(data_encoded[["age", "smoker_yes"]].corr())
    filtered_data = data_encoded[relevant_features.keys()]
    print(filtered_data.keys())
    # TODO Preise Logarithmieren
    # Auf medizindaten.
    y = df_data['charges']
    ## Preprocessing & Training using GridSearch with multiple models
    mm_gs_res = mm_gridsearch(filtered_data.to_numpy(), y.to_numpy(), r_models1, r_params1)
    print(mm_gs_res.score_summary()[['estimator', 'mean_score', 'n_estimators']])
    print("Placeholder - Miri Choi Dataset")


# On original data seems to perform well!
# Uses ColumnTransformer to process data.
def miri_preprocess_deprecated(df_data):
    categorical_features = ['region', 'sex', 'smoker']
    numerical_features = ['age', 'bmi', 'children'] # Exclude target
    preprocessed_data, preprocessor = transformations(df_data, categorical_features, numerical_features)
    # y = preprocessed_data['num__charges'] # Do not do this...
    # y = df_data['charges']  #Could apply log here
    ## Preprocessing & Training using GridSearch with multiple models
    # mm_gs_res = mm_gridsearch(preprocessed_data.to_numpy(), y.to_numpy(), r_models1, r_params1)
    # gridsearch_regression(preprocessed_data, df_data['charges'])
    # mm_gs_res.grid_searches['XGBRegressor'].predict(test[0].to_numpy()[1336].reshape(1,11)) # predict...
    preprocessed_data['charges'] = df_data['charges']
    return preprocessed_data, preprocessor


def miri_preprocess2(df_data,categorical_features=['smoker', 'region', 'sex'],numerical_features = ['age', 'bmi', 'children', 'charges']):
    preprocessed_data, preprocessor = transformations(df_data,categorical_features, numerical_features)
    # y = preprocessed_data['num__charges'] # Do not do this...
    # y = df_data['charges']  #Could apply log here
    ## Preprocessing & Training using GridSearch with multiple models
    # mm_gs_res = mm_gridsearch(preprocessed_data.to_numpy(), y.to_numpy(), r_models1, r_params1)
    # gridsearch_regression(preprocessed_data, df_data['charges'])
    # mm_gs_res.grid_searches['XGBRegressor'].predict(test[0].to_numpy()[1336].reshape(1,11)) # predict...
    # preprocessed_data['charges'] = df_data['charges']
    return preprocessed_data, preprocessor


def miri_preprocess3(df_data, col_names=[ 'children', 'age', 'bmi', 'region', 'charges'], totype=['int', 'float', 'float', 'category', 'float']):
    res = pd.DataFrame()
    res['smoker'] = df_data.replace({'smoker': {'yes': True, 'no': False}})['smoker']
    res['sex'] = df_data.replace({'sex': {'female': True, 'male': False}})['sex']

    if not(len(col_names) == len(totype)):
        input("Provided columns and types not equal length...")


    for i, col_name in enumerate(col_names):
        res[col_name] = df_data[col_name].astype(totype[i])


    return res

def miri_preprocess_pct(df_syn_pre):
    df_syn_pre['smoker'] = df_syn_pre.replace({'smoker': {1: 'yes', 0: 'no'}})['smoker']
    df_syn_pre['sex'] = df_syn_pre.replace({'sex': {1: 'female', 0: 'male'}})['sex']
    pre = miri_preprocess3(df_syn_pre,col_names=[ 'children', 'age', 'bmi', 'charges'], totype=['int', 'float', 'float', 'float'])
    transformed = miri_preprocess2(pre,categorical_features=['smoker', 'sex'], numerical_features = ['age', 'bmi', 'children', 'charges'])
    return transformed

def gridsearch_regression(df_X, df_y, scoring='r2'):
    mm_gs_res = mm_gridsearch(df_X.to_numpy(), df_y.to_numpy(), r_models1, r_params1, scoring=scoring)
    return mm_gs_res

def gridsearch_classification(df_X, df_y, scoring='roc_auc'):
    mm_gs_res = mm_gridsearch(df_X.to_numpy(), df_y.to_numpy(), c_models1, c_params1, scoring=scoring)
    return mm_gs_res


def test_ctgan(df_orig, paths, sample=True, n_samples=1000):
    networks = []
    eval_res = []
    for i, path in enumerate(paths):
        networks.append(load_ctgan(path))
        if sample:
            aug_samples = networks[i].sample(n_samples)
            eval_res.append(evaluate(aug_samples, df_orig))
    return networks, eval_res

def sample_multiple(models, n_samples):
    res = []
    for model in models:
        res.append(model.sample(n_samples))

    return res

def plot_models_dist(models):
    for model in models:
        plt.figure("Distribution Plots")
        sns.distplot(df_data['charges'], label='Real data')
        sns.distplot(aug_data['charges'], label='Synthetic data')
        plt.legend()
        plt.savefig('imgs/miri_dp_e1000_b200.png')

def filter_feats(df_data, get_feats = ['age', 'bmi', 'children', 'charges', 'smoker']):
    return df_data.copy()[get_feats]

# FOR MIRICHOI
def do_pipeline(df_data): # Uncleaned Mirichoi dataframe
    pre = miri_preprocess3(df_data)
    transformed = miri_preprocess2(pre)
    return transformed

# ctgan pipeline
def pipe_ctgans(modelpaths=[], n_samples=2000, df_orig=None):
    for modelpath in modelpaths:
        ctgan = load_ctgan(modelpath)
        df_ctgan = ctgan.sample(n_samples)
        syn_ctgan = do_pipeline(df_ctgan)
        X, y = split(syn_ctgan[0])
        mm_gs_res = gridsearch_regression(X, y)
        name = gen_filename(mm_gs_res.score_summary().to_string(), opt='ctgan_{}_{}'.format(ctgan._model_kwargs['batch_size'], ctgan._model_kwargs['epochs']))
        mm_gs_res.score_summary().to_csv(os.path.join('my_results/', name) + '.csv')
        cor = miri_preprocess3(df_ctgan).corr()
        #plt.close()
        fig = plt.figure()
        fig.clear()
        sns_plot = sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
        # print(results_path)
        pname = '{}_pearson'.format(name)
        plt.savefig(os.path.join('my_results/', pname))
        if df_orig is not None:
            res = evaluate(df_ctgan, df_orig)
            fh = open(os.path.join('my_results/', name) + '_evaluate.txt', 'w')
            fh.write(str(res))
            fh.close()
            fig.clear()
            # plt.close()
            sns.distplot(df_orig['charges'], label='Real data')
            sns.distplot(df_ctgan['charges'], label='Synthetic data')
            plt.legend()
            plt.savefig(os.path.join('my_results/', name+'_distribution'))

def smote(df_data, y_label='readmitted'):
    sm = SMOTE(random_state=42)
    X,y = split(df_data, pkey=y_label)
    syn_X, syn_y = sm.fit_resample(X, y)
    return syn_X, syn_y

def do_evaluations_readd(n_samples=10000):
    df_data = get_dataset('data/kaagle_hops_readmission/train.csv')
    # network_paths = ['models/readd_ctgan__wth_target_e1000_b200.pkl', 'models/readd_tvae_i_wth_target_e1000_b500.pkl','readd_smote_readmitted_def']
    network_paths = ['readd_smote_readmitted_def']
    X, y = None, None
    name = "undefinedweirdthing"
    for path in network_paths:
        if 'smote' in path:
            X, y = smote(df_data)
            df_syn = X.copy()
            df_syn['readmitted'] = y
            res = evaluate(df_syn, df_data,metrics=['CSTest', 'KSTest', 'BNLogLikelihood','LogisticDetection'])
            name = gen_filename('Smotehash',
                                opt='model_{}'.format(path.split("_")[1]))
            fh = open(os.path.join('my_results/', name) + '_evaluate.txt', 'w')
            fh.write(str(res) + "\n" + path)
            fh.close()
            name = 'Smoti'
            print("Smote found")
        else:
            net_path_handle = open(path, 'rb')
            model = pickle.load(net_path_handle)
            net_path_handle.close()

            df_syn = model.sample(n_samples)
            print("Evaluating: {}".format(path))
            res = evaluate(df_syn, df_data)
            X, y = split(df_syn, 'readmitted')


        mm_gs_res = gridsearch_classification(X, y)

        name = gen_filename(mm_gs_res.score_summary().to_string(),
                            opt='model_{}'.format(path.split("_")[1]))
        fh = open(os.path.join('my_results/', name) + '_evaluate.txt', 'w')
        fh.write(str(res) + "\n" + path)
        fh.close()
        mm_gs_res.score_summary().to_csv(os.path.join('my_results/', name + "score_summary") + '.csv')
        threshold = 0.1
        rel_feats = pearson_target(df_syn, target_key='readmitted', threshold=threshold)
        while rel_feats.shape[0] <= 1:
            threshold = threshold / 1.25
            rel_feats = pearson_target(df_syn, target_key='readmitted', threshold=threshold)
        cor = df_syn[rel_feats.keys()].corr()
        fig = plt.figure()
        fig.clear()
        sns_plot = sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
        pname = '{}_pearson'.format(name)
        plt.savefig(os.path.join('my_results/', pname))

def main_mirichoi():
    # Calculate Charges based on various features.
    ## Feature Selection
    df_data = get_dataset('data/kaagle_mirichoi2018/insurance.csv')
    # df_data_booled = df_data.replace({'smoker': {'yes': True, 'no': False}})
    # fit_tgan(df_data, ['age','bmi','children', 'charges'], 'models/tgan_def.pkl')
    # df_data['sex'] = df_data['sex'].astype('bool')
    # print("With custom preoprocessing")
    # mirichoi_train(df_data)
    pipe_ctgans(['models/ctgan_i_plain_wth_target_e10000_b100.pkl'], df_orig=df_data)
    print("With preprocessing pipeline")
    # prep_tup = miri_preprocess2(df_data)
    # fit_tgan(prep_tup[0], ['age', 'bmi', 'children', 'charges'], 'models/tgan_def_colpreprocess_with_fullPreprocess.pkl')
    # fit_tgan(prep_tup[0], ['num__age', 'num__bmi', 'num__children', 'num__charges'],'models/tgan_def_with_fullPreprocess.pkl')
    # fit_ctgan(prep_tup[0],'models/ctgan_i_plain_wth_target.pkl')
    # fit_ctgan(df_data, 'models/ctgan_i_plain_wth_target_e4000_b100.pkl', epochs=4000, bsize=100)
    # fit_ctgan(df_data, 'models/ctgan_i_plain_wth_target_e10000_b50.pkl', epochs=10000, bsize=50)
    # fit_ctgan(df_data, 'models/ctgan_i_plain_wth_target_e10000_b100.pkl', epochs=10000, bsize=100)
    # fit_ctgan(df_data, 'models/ctgan_i_plain_wth_target_e10000_b25.pkl', epochs=10000, bsize=20)
    # fit_ctgan(df_data, 'models/ctgan_ii_smbool_wth_target_e1000_b200.pkl', epochs=1000, bsize=200)
    # fit_tvae(df_data, 'models/tvae_i_wth_target_e1000_b500.pkl', epochs=1000, bsize=500)
    smogn_test = fit_smogn_regress(df_data)
    # fil_data = filter_feats(df_data)
    #fit_ctgan(fil_data, 'models/ctgan_ii_smbool_wth_target_filteredtest_e1000_b200.pkl', epochs=1000, bsize=200)
    # df = df_data.replace({'smoker': {'yes': True, 'no': False}})
    print("Now on augmented data from CTGAN")
    ctgan_paths = ['models/ctgan_i_plain_wth_target_e4000_b100.pkl',    # 0
                   'models/ctgan_i_plain_wth_target_e10000_b50.pkl',    # 1
                   'models/ctgan_i_plain_wth_target_e10000_b100.pkl',   # 2
                   'models/ctgan_i_plain_wth_target_e10000_b25.pkl',    # 3
                   'models/ctgan_i_plain_wth_target_e3000_b100.pkl',    # 4
                   'models/ctgan_i_plain_wth_target_e1000_b200.pkl',    # 5 Yields best results 0.64
                   'models/ctgan_i_plain_wth_target.pkl']
    # ctgan_paths = ['models/ctgan_i_plain_wth_target_e1000_b200.pkl','models/tvae_i_wth_target_e1000_b500.pkl']               # 6

    #ctgan_paths_smoker_bool = ['models/ctgan_ii_smbool_wth_target_filteredtest_e1000_b200.pkl',   # 0 NaN cuz feats missing and ?
    #                'models/ctgan_ii_smbool_wth_target_e1000_b200.pkl']   # 1
    print("Now on 1000 samples")

    ctgans, eval_results = test_ctgan(df_data, ctgan_paths, n_samples=1000) # Retrieves networks and yields synthetic data score
    print("CTGANs Plain {} \n {}".format(1000, eval_results))
    aug_data = tvae.sample(2000) #TVAE
    plt.figure("Distribution Plots")
    sns.distplot(df_data['charges'], label='Real data')
    sns.distplot(aug_data['charges'], label='Synthetic data')
    plt.legend()
    plt.savefig('imgs/miri_tvae_e1000_b500.png')
    syn = do_pipeline(aug_data)  # Prepares all datasets for the machine learning pipeline
    X, y = split(syn[0])
    mm_gs_res = gridsearch_regression(X, y)
    print(mm_gs_res.score_summary()[['estimator', 'mean_score', 'n_estimators']])
    # print("CTGANs Smoker Booled {} \n {}".format(1000, eval_results_sm))
    # ctgans2, eval_results2 = test_ctgan(df_data, ctgan_paths, n_samples=10000)
    # ctgans_sm2, eval_results_sm2 = test_ctgan(df_data_booled, ctgan_paths_smoker_bool, n_samples=10000)
    # print("CTGANs Plain {} \n {}".format(10000, eval_results2))
    # print("CTGANs Smoker Booled {} \n {}".format(10000, eval_results_sm2))
    #ctgan = load_ctgan('models/ctgan_i_plain_wth_target_e3000_b100.pkl')
    # sns.distplot(df_data['charges']) #plot distribution
    #gridsearch_regression(prep_tup[0], df_data['charges'])
    # aug_data = ctgans[5].sample(1000)
    print("STOP HERE")
    # plt.figure("Distribution Plots")
    # sns.distplot(df_data['charges'], label='Real data')
    # sns.distplot(aug_data['charges'], label='Synthetic data')
    # plt.legend()
    # plt.savefig('imgs/miri_dp_e1000_b200.png')
    # plt.plot()
    # prep_tup[0].iloc[0][['num__age','num__bmi','num__children','num__charges']]   #Gets one samples nuemrical values
    # prep_tup[1].transformers_[0][1].inverse_transform(prep_tup[0].iloc[0][['num__age','num__bmi','num__children','num__charges']]) # revert scaling
    print("Now on augmented data from TGAN")
    tgan = load_tgan('models/tgan_def_with_fullPreprocess.pkl')
    df_data = tgan.sample(1500)
    print(df_data)
    #mm_gs_res = gridsearch_regression(df_data, y)
    # print(mm_gs_res.score_summary()[['estimator', 'mean_score', 'n_estimators']])
    # mirichoi_train(df_data)

    #print("Now on augmented data")
    #data = get_ctgan(n_samples=1000, name='models/mirichoi_std_1')
    #df_data_aug = pd.DataFrame(data, columns=df_data.keys())
    #mirichoi_train(df_data_aug)
    # print(data_encoded.keys())
    pass

def eval_miri_patectgan_df(path ='data/miri_pategan_synthetic_data_e1.csv', target_key='num__charges'):
    df_syn = get_dataset(path)
    # del df_syn['Unnamed: 0']
    # df_syn = miri_continous(df_syn)
    df_syn_pre = df_syn
    # df_syn,transformers = do_pipeline(df_syn)
    df_syn, transformers = miri_preprocess_pct(df_syn) # Try
    X, y = split(df_syn, target_key)
    # Data comes already preprocessed so only need to split into X,y start training
    # maybe need to exclude IDs generated by pategan
    mm_gs_res = gridsearch_regression(X, y)
    name = gen_filename(mm_gs_res.score_summary().to_string(),
                        opt='model_{}'.format('miri_patectgan'))


    mm_gs_res.score_summary().to_csv(os.path.join('my_results/', name + "score_summary") + '.csv')
    threshold = 0.1
    rel_feats = pearson_target(df_syn, target_key=target_key, threshold=threshold)
    while rel_feats.shape[0] <= 1:
        threshold = threshold / 1.25
        rel_feats = pearson_target(df_syn, target_key=target_key, threshold=threshold)
    cor = df_syn[rel_feats.keys()].corr()
    fig = plt.figure()
    fig.clear()
    sns_plot = sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
    pname = '{}_pearson'.format(name)
    plt.savefig(os.path.join('my_results/', pname))

    fh = open(os.path.join('my_results/', name) + '_evaluate.txt', 'w')
    df_data = miri_preprocess3(get_dataset('data/kaagle_mirichoi2018/insurance.csv'))
    del df_data['region'] # Makes evaluation possible, with equal features
    del df_syn_pre['region']
    res = evaluate(df_syn_pre, df_data)
    res2 = evaluate(df_syn_pre, df_data,
                    metrics=['CSTest', 'KSTest', 'BNLogLikelihood', 'LogisticDetection', 'GMLogLikelihood',
                             'CategoricalCAP', 'NumericalLR', 'NumericalMLP', 'NumericalRadiusNearestNeighbor'])
    fh.write(str(res) + "\n" + path)
    fh.close()
    sns.distplot(df_data['charges'], label='Real data')
    sns.distplot(df_syn_pre['charges'], label='Synthetic data')
    plt.legend()
    plt.savefig(os.path.join('my_results/', name) + '_dist.png')

def eval_read_patectgan_df(path = 'data/read_test2read_20210617_095304_synthetic_data.csv', target_key='readmitted'):
    df_syn = get_dataset(path)

    X,y = split(df_syn,target_key)
    # Data comes already preprocessed so only need to split into X,y start training
    # maybe need to exclude IDs generated by pategan
    mm_gs_res = gridsearch_classification(X, y)
    name = gen_filename(mm_gs_res.score_summary().to_string(),
                        opt='model_{}'.format('read_patectgan'))
    # fh = open(os.path.join('my_results/', name) + '_evaluate.txt', 'w')
    # res = evaluate(df_syn, df_data)
    # fh.write(str(res) + "\n" + path)
    # fh.close()
    mm_gs_res.score_summary().to_csv(os.path.join('my_results/', name + "score_summary") + '.csv')
    threshold = 0.1
    rel_feats = pearson_target(df_syn, target_key=target_key, threshold=threshold)
    while rel_feats.shape[0] <= 1:
        threshold = threshold / 1.25
        rel_feats = pearson_target(df_syn, target_key=target_key, threshold=threshold)
    cor = df_syn[rel_feats.keys()].corr()
    fig = plt.figure()
    fig.clear()
    sns_plot = sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
    pname = '{}_pearson'.format(name)
    plt.savefig(os.path.join('my_results/', pname))
    fh = open(os.path.join('my_results/', name) + '_evaluate.txt', 'w')
    df_data = get_dataset('data/kaagle_hops_readmission/train.csv')
    data_columns = [col for col in df_data.columns if col != target_key]
    df_data = df_data[data_columns]
    res = evaluate(df_syn, df_data) # res = evaluate(df_syn, df_data,metrics=['CSTest', 'KSTest', 'BNLogLikelihood','LogisticDetection'])  #produces error 'CategoricalEnsemble'
    res2 = evaluate(df_syn, df_data, metrics=['CSTest', 'KSTest', 'BNLogLikelihood', 'LogisticDetection', 'GMLogLikelihood', 'CategoricalCAP', 'NumericalLR', 'NumericalMLP', 'NumericalRadiusNearestNeighbor'])
    #
    fh.write(str(res) + "\n" + str(res2) + "\n" + path)
    fh.close()

def convert_mirichoi2018(df):
    df_c = df.copy()
    cols = ['charges', 'age', 'bmi', 'children']
    df_c[cols] = df_c[cols].astype(float)
    return df_c

#Format fields of miri to floats
def miri_continous(df, types=['float', 'float', 'float', 'float', 'float','float']):
    res = df.copy()
    for i, col_name in enumerate(df.keys()):
        res[col_name] = df[col_name].astype(types[i])
    return res

def prep_pategan_data():
    df_data = get_dataset('data/kaagle_mirichoi2018/insurance.csv')
    # determine the path where to save the train and test file
    df_prep = miri_preprocess3(df_data, col_names=['children', 'age', 'bmi', 'region', 'charges'],
                               totype=['int', 'int', 'int', 'category', 'int'])
    del df_prep['region']
    df_prep['smoker'] = df_data.replace({'smoker': {'yes': 1, 'no': 0}})['smoker'].astype('int')
    df_prep['sex'] = df_data.replace({'sex': {'female': 1, 'male': 0}})['sex'].astype('int')
    train, test = train_test_split(df_prep, test_size=0.2, random_state=42)
    # save the train and test file
    # again using the '\t' separator to create tab-separated-values files
    train.to_csv('data/miri_train_nopreprocess.csv', index=False)
    test.to_csv('data/miri_test_nopreprocess.csv', index=False)
    print("Done Preparing for PateGAN")

def prep_pategan_data_discrete():
    # df_data = get_dataset('data/data/kaagle_hops_readmission/train.csv')
    # determine the path where to save the train and test file
    df_prep = miri_preprocess3(df_data, col_names=['children', 'age', 'bmi', 'region', 'charges'],
                               totype=['int', 'int', 'int', 'category', 'int'])
    del df_prep['region']
    df_prep['smoker'] = df_data.replace({'smoker': {'yes': 1, 'no': 0}})['smoker'].astype('int')
    df_prep['sex'] = df_data.replace({'sex': {'female': 1, 'male': 0}})['sex'].astype('int')
    train, test = train_test_split(df_prep, test_size=0.2, random_state=42)
    # save the train and test file
    # again using the '\t' separator to create tab-separated-values files
    train.to_csv('data/miri_train_nopreprocess.csv', index=False)
    test.to_csv('data/miri_test_nopreprocess.csv', index=False)
    print("Done Preparing for PateGAN")

# USE THIS TO GENERATE SYNTHETIC DATA FOR MIRICHOI
def generate_syn_miri(path_miri='data/kaagle_mirichoi2018/insurance.csv', network_paths = ['models/abgabe_smote_def.pkl'],path_patectgan_syn=None, n_samples=10000, fit=False):

    name = "abgabe_pipeline"
    run_name = gen_filename(name)

    #####- MIRICHOI 2018 -########
    # Load Dataframe and preprocess:
    df_miri = get_dataset(path_miri)  # Loads dataset
    print("Loaded Data - Running ML Pipeline on Synthetic Samples")
    # Create Models with functions of helpers.py,
    if fit:
        from datetime import datetime
        date_time_obj = datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S')
        epochs = 1000  # TODO
        b_size = 200  # TODO
        ctgan_name = "models/{}_miri_ctgan_abgabe_e{}_b{}.pkl".format(date_time_obj, epochs, b_size)
        fit_ctgan(df_miri, ctgan_name, epochs=epochs, bsize=b_size)
        tvae_name = "models/{}_miri_tvae_abgabe_e{}_b{}.pkl".format(date_time_obj, epochs, b_size)
        fit_tvae(df_miri, tvae_name, primkey=None, epochs=epochs, bsize=b_size)
        print("Done Training Mirichoi - exiting, start again with fit=False and add created models path to network_paths")
        return True

    ## CTGAN, TVAE
    ## ## Use load_ctgan to load model if required, multple trained models can be provided as path to pipe_ctgans
    # All pipeline results are saved to ./my_results/, e.g pearson, data distribution, evaluations
    # Metrics can fail due to default scoring functions...
    # df_orig is the original dataset to calculate evaluation metrics
    for netpath in network_paths:
        if 'ctgan' in netpath:
            ## CTGAN PART ##
            pipe_ctgans([netpath],n_samples=n_samples, df_orig=df_miri)
        elif 'tvae' in netpath:
            ## TVAE PART ##
            tvae = TVAE.load(netpath)
            syn_tvae = tvae.sample(n_samples)
            plt.figure("Distribution Plots")
            sns.distplot(df_miri['charges'], label='Real data')
            sns.distplot(syn_tvae['charges'], label='Synthetic data')
            plt.legend()
            name = os.path.basename(os.path.normpath(netpath)).replace('.', '')
            plt.savefig('my_results/{}_distribution.png'.format(name))
            syn = do_pipeline(syn_tvae)  # Prepares dataset for machine learning pipeline
            X_syn, y_syn = split(syn[0])
            mm_gs_res = gridsearch_regression(X_syn, y_syn)  # Run ML Pipeline for regression tasks with synthetic data
            mm_gs_res.score_summary().to_csv(os.path.join('my_results/', name + "_score_summary") + '.csv')
            print(mm_gs_res.score_summary()[['estimator', 'mean_score', 'n_estimators']][:5])
            res = evaluate(syn_tvae, df_miri)
            fh = open(os.path.join('my_results/', name) + '_evaluate.txt', 'w')
            fh.write(str(res))
            fh.close()
            plt.close("Distribution Plots")
            cor = miri_preprocess3(syn_tvae).corr()
            fig = plt.figure()
            fig.clear()
            sns_plot = sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
            # print(results_path)
            pname = '{}_pearson'.format(name)
            plt.savefig(os.path.join('my_results/', pname))

        elif 'smogn' in netpath:
            ### SMOGN PART ####
            # X,y = split(df_miri, 'charges')
            smogn = fit_smogn_regress(df_miri, y_label='charges')
            smogn_preped = do_pipeline(smogn)  # Prepares dataset for machine learning pipeline
            # Second idx are the transformers to calculate back the values if one wants to
            X_smogn, y_smogn = split(smogn_preped[0])
            mm_gs_res = gridsearch_regression(X_smogn,
                                              y_smogn)  # Run ML Pipeline for regression tasks with synthetic data
            mm_gs_res.score_summary().to_csv(os.path.join('my_results/', run_name + "_smogn_pipeline") + '.csv')
            print(mm_gs_res.score_summary()[['estimator', 'mean_score', 'n_estimators']][:5])
            print("SMOGN Regressor - Done")


        print("Finished with: {}".format(netpath))

    #### PATECTGAN ####
    # TODO USE the minitry.py file and its according functions for training, saving a model and further generating a new dataset
    # See imports above for further information
    if path_patectgan_syn is not None:
        syn_patectgan = get_dataset(path_patectgan_syn)
        plt.figure("Distribution Plots")
        sns.distplot(df_miri['charges'], label='Real data')
        sns.distplot(syn_patectgan['charges'], label='Synthetic data')
        plt.legend()
        plt.savefig('imgs/miri_patectgan_abgabe_default.png')
        syn = miri_preprocess2(syn_patectgan,
                               categorical_features=['smoker',
                                                     'sex'])  # Prepares dataset for machine learning pipeline
        # syn = miri_continous()
        X_syn, y_syn = split(syn[0])
        mm_gs_res = gridsearch_regression(X_syn, y_syn)  # Run ML Pipeline for regression tasks with synthetic data
        mm_gs_res.score_summary().to_csv(os.path.join('my_results/', run_name + "_patectgan_pipeline") + '.csv')
        print(mm_gs_res.score_summary()[['estimator', 'mean_score', 'n_estimators']][:5])
        print("Done with synthetic data PATECTGAN")
    else:
        print(
            "Skipping PATECTGAN, please provide path to synthetic data csv. See /patectgan/patectgangym/minitry.py")

    return True
    #####- MIRICHOI 2018 END -########
    ##
    ##

def generate_syn_read(path_read='data/kaagle_hops_readmission/train.csv',network_paths = ['models/abgabe_smote_def.pkl'], n_samples=10000, fit=False):

    df_read = get_dataset(path_read)  # Loads dataset

    # Fit Models
    if fit:
        from datetime import datetime
        date_time_obj = datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S')
        epochs = 1000
        b_size = 200
        ctgan_name = "models/{}_readd_ctgan_abgabe_e{}_b{}.pkl".format(date_time_obj, epochs, b_size)
        fit_ctgan(df_read, ctgan_name, epochs=epochs, bsize=b_size)
        print("Fitted CTGAN - Epochs: {} , Batch Size: {}".format(epochs, b_size))
        epochs = 1000
        b_size = 100
        tvae_name = "models/{}_readd_tvae_abgabe_e{}_b{}.pkl".format(date_time_obj, epochs, b_size)
        fit_tvae(df_read, tvae_name, epochs=epochs, bsize=b_size)
        print("Fitted TVAE - Epochs: {} , Batch Size: {}".format(epochs, b_size))
        print("Done Training Readmission - exiting function, start again with fit=False and add created models path to network_paths")
        return True

    X, y = None, None
    mtype = 'unknown'
    for path in network_paths:
        if 'smote' in path:
            X, y = smote(df_read)
            df_syn = X.copy()
            df_syn['readmitted'] = y
            res = evaluate(df_syn, df_read, metrics=['CSTest', 'KSTest', 'BNLogLikelihood', 'LogisticDetection'])
            mtype = 'smote'
            print("Smote found")
        else:
            net_path_handle = open(path, 'rb')
            model = pickle.load(net_path_handle)
            net_path_handle.close()
            df_syn = model.sample(n_samples)
            print("Evaluating: {}".format(path))
            res = evaluate(df_syn, df_read, metrics=['CSTest', 'KSTest', 'BNLogLikelihood', 'LogisticDetection'])
            # res = evaluate(df_syn, df_read) # TODO: TVAE, need take care of used metrics, measuring otherwise fails and takes endless
            X, y = split(df_syn, 'readmitted')
            if 'ctgan' in path:
                mtype = 'ctgan'
            elif 'tvae' in path:
                mtype = 'tvae'

        mm_gs_res = gridsearch_classification(X, y)
        name = gen_filename(mm_gs_res.score_summary().to_string(),
                            opt='model_{}'.format(mtype))
        fh = open(os.path.join('my_results/', name) + '_evaluate.txt', 'w')
        fh.write(str(res) + "\n" + path)
        fh.close()
        mm_gs_res.score_summary().to_csv(os.path.join('my_results/', name + "score_summary") + '.csv')
        threshold = 0.1
        rel_feats = pearson_target(df_syn, target_key='readmitted', threshold=threshold)
        while rel_feats.shape[0] <= 1:
            threshold = threshold / 1.25
            rel_feats = pearson_target(df_syn, target_key='readmitted', threshold=threshold)
        cor = df_syn[rel_feats.keys()].corr()
        fig = plt.figure()
        fig.clear()
        sns_plot = sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
        pname = '{}_pearson'.format(name)
        plt.savefig(os.path.join('my_results/', pname))
        plt.close()

def generate_from_scratch():
    #####- PATECTGAN  SETS -########
    # TODO: create synthetic datasets using the script: code\patectgan\patectsdgym\minitry
    # TODO: Use the functions miri() and read() to collect models and further synthetic datasets.
    # TODO: Requires package: ctgan v0.2.2 in patectgan environment for compatibility
    # TODO: Then use following functions to produce evaluations with the created models

    #TODO: eval_miri_patectgan_df('data/syn_miri_patectgan_e5.csv')
    #TODO: eval_read_patectgan_df('data/syn_read_patectgan_e12_part.csv', 'readmitted')
    print("Eval PATECTGAN TEST")
    #####- MIRICHOI 2018 -########
    path_miri = 'data/kaagle_mirichoi2018/insurance.csv'
    ### df_path_patectgan = 'data/syn_miri_patectgan_e5.csv'

    # Please make sure that modelspath contains model type name, e.g CTGAN -> blabla_ctgan_def.pkl'
    # Only for CTGAN and TVAE in case of mirichoi
    network_paths = ['models/20210728_142428_miri_ctgan_abgabe_e1000_b200.pkl',
                     'models/20210728_142428_miri_tvae_abgabe_e1000_b200.pkl',
                     'do_smogn']
    n_samples = 5000
    fit = False
    generate_syn_miri(path_miri, network_paths, None, n_samples, fit)  #SMOGN, CTGAN, TVAE

    ####- Readmission -############
    path_read = 'data/kaagle_hops_readmission/train.csv'
    network_paths = ['models/readd_ctgan_def_abgabe_wth_target_e1000_b200.pkl',
                     'models/readd_tvae_def_abgabe_wth_target_e1000_b500.pkl',
                     'models/abgabe_smote_def.pkl']
    n_samples = 5000
    fit = False
    generate_syn_read(path_read,network_paths,n_samples,fit)  #SMOGN, CTGAN, TVAE


if __name__ == "__main__":
    # loop_datasets()
    # main_synthea()
    # main_mirichoi()
    # main_readd()
    # prep_pategan_data()
    ### do_evaluations_readd(n_samples=10000)
    #eval_read_pategan_df(
    #    'H:/Studium/seminar_business_analytics/code/patectgan/patectsdgym/syn_read_patectgan_e12part.csv',
    #    'readmitted')
    # eval_miri_patectgan_df('H:/Studium/seminar_business_analytics/code/patectgan/patectsdgym/syn_patectgan_e12.csv', 'num__charges')
    #eval_miri_pategan_df('H:/Studium/seminar_business_analytics/code/patectgan/patectsdgym/syn_miri_patectgan_e5.csv',
     #                    'charges')
    # eval_read_pategan_df('H:/Studium/seminar_business_analytics/code/data/kaagle_hops_readmission/syn_read_patectgan_e15_part.csv', 'readmitted')
    # main_mirichoi()
    generate_from_scratch()

    print("Done")
