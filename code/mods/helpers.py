import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import smogn as smogn
from sdv.tabular import CTGAN, TVAE
import pickle
from sklearn import preprocessing
import numpy as np

# NOT WORKING
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, \
    GradientBoostingClassifier
from sklearn.linear_model import SGDRegressor, Ridge, HuberRegressor, LinearRegression
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC, SVR
import xgboost
from xgboost import XGBRegressor, XGBClassifier
from tgan.model import TGANModel
#from table_evaluator import load_data, TableEvaluator
from sdv.evaluation import evaluate

from EstimationSelectionHelper import EstimatorSelectionHelper
from Preprocessing import transformations


def split(df_data,pkey='num__charges'):
    y = df_data[pkey]
    # del df_data[pkey]
    X = df_data.copy()
    del X[pkey]
    return (X,y)


def fit_smogn_regress(df_data, y_label='charges'):
    smogn_net = smogn.smoter(df_data, y=y_label)
    # smogn_net.sample(n_samples)
    return smogn_net

def fit_tgan(df_data, cont_colums=None, model_path='models/tgan_def.pkl'):
    # import tensorflow as tf
    #tf.compat.v1.disable_eager_execution()
    tgan = TGANModel(cont_colums)# ,gpu='/GPU:0')
    tgan.fit(df_data)
    tgan.save(model_path)
    return tgan

def load_tgan(model_path='models/tgan_def.pkl'):
    tgan = TGANModel.load(model_path)
    return tgan

def _donotplot_grid_search(cv_results, grid_param_1, grid_param_2, name_param_1, name_param_2):
    # Get Test Scores Mean and std for each grid search
    scores_mean = cv_results['mean_test_score']
    #scores_mean = np.array(scores_mean).reshape(len(grid_param_2.unique()),len(grid_param_1.unique()))

    scores_sd = cv_results['std_test_score']
    #scores_sd = np.array(scores_sd).reshape(len(grid_param_2),len(grid_param_1))

    # Plot Grid search scores
    _, ax = plt.subplots(1,1)

    # Param1 is the X-axis, Param 2 is represented as a different curve (color line)
    for idx, val in enumerate(grid_param_2):
        ax.plot(grid_param_1, scores_mean.iloc[idx], '-o', label= name_param_2 + ': ' + str(val))

    ax.set_title("Grid Search Scores", fontsize=20, fontweight='bold')
    ax.set_xlabel(name_param_1, fontsize=16)
    ax.set_ylabel('CV Average Score', fontsize=16)
    ax.legend(loc="best", fontsize=15)
    ax.grid('on')

def plot_config(cv_results, param_1):
    scores_mean = cv_results['mean_test_score']
    # scores_mean = np.array(scores_mean).reshape(len(grid_param_2.unique()),len(grid_param_1.unique()))

    scores_sd = cv_results['std_test_score']
    # Plot Grid search scores
    _, ax = plt.subplots(1, 1)

    # Param1 is the X-axis, Param 2 is represented as a different curve (color line)
    for idx, val in enumerate(cv_results.params):
        ax.plot(idx,scores_mean.iloc[idx], '-o', label=str(val))

    ax.set_title("Grid Search Scores", fontsize=20, fontweight='bold')
    ax.set_xlabel("Configs", fontsize=16)
    ax.set_ylabel('CV Average Score', fontsize=16)
    ax.legend(loc="best", fontsize=15)
    ax.grid('on')
    plt.show()

def preprocess_scale(X):
    return preprocessing.scale(X)


def save_model(model, filename):
    with open(filename, 'wb') as file:
        pickle.dump(model, file)

def load_model(filename):
    with open(filename, 'rb') as file:
        pickle_model = pickle.load(file)
    return pickle_model


def pca_dim_reduction(x, n_comp,random_state=42):
    from sklearn.decomposition import PCA
    pca = make_pipeline(StandardScaler(),
                        PCA(n_components=n_comp, random_state=random_state))
    principalComponents = pca.fit_transform(x)
    principalDf = pd.DataFrame(data=principalComponents)
    return principalDf

def plot_data(X,y):
    plt.scatter(X[:, 0], X[:, 1], marker='o', c=y,
                s=25, edgecolor='k')
    plt.show()

def plot_data_r(X,y):
    plt.plot(X, y, '-o')
    plt.show()

def plot_pca_c(X,y):
    fig = plt.figure()
    ax2 = fig.add_subplot(122)
    ax2.scatter(X[:, 0], X[:, 1], marker='.', c=y,
                s=25, edgecolor='k')
    plt.show()

def plot_pca_r(X,y):
    fig = plt.figure()
    ax2 = fig.add_subplot(122)
    ax2.scatter(X[:, 0], X[:, 1], marker='.', c=y,
                s=25, edgecolor='k')
    plt.show()

def fit_smote(data, name='smote_basic.pkl'):
    sm = SMOTE(sampling_strategy='all')
    X,y = sm.fit_resample(data[0], data[1])
    save_model(sm, name)
    return X,y

def fit_ctgan(data, name,primkey=None, epochs=1000,bsize=100):
    #model = CTGAN(epochs=300, cuda=False, generator_dim=(256, 256, 256),
    #              discriminator_dim=(256, 256, 256),primary_key=primkey)
    model = CTGAN(epochs=epochs,batch_size=bsize)
    if primkey is not None:
        model = CTGAN(epochs=epochs,batch_size=bsize,primary_key=primkey)

    model.fit(data)
    model.save(name)
    return model

def fit_tvae(data, name,primkey=None, epochs=1000,bsize=100):
    #model = CTGAN(epochs=300, cuda=False, generator_dim=(256, 256, 256),
    #              discriminator_dim=(256, 256, 256),primary_key=primkey)
    model = TVAE(epochs=epochs,batch_size=bsize)
    if primkey is not None:
        model = TVAE(epochs=epochs,batch_size=bsize,primary_key=primkey)

    model.fit(data)
    model.save(name)
    return model

def load_ctgan(name='my_model_1.pkl'):
    model = CTGAN.load(name)
    return model

def get_dataset(filepath, yLabel=None):
    data = pd.read_csv(filepath, encoding="ISO-8859-1")
    y = None
    X = None
    if yLabel is not None:
        y = data[yLabel]  # Get labels
        base_features = [c for c in data.columns if c != yLabel]  # Extract all other features
        X = data[base_features]  # Collect features for all samples in x
        return X, y
    else:
        return data

def choose_as_label(data, feature='y', filter=['Id']):
    y = data[feature]  # Get labels
    base_features = [c for c in data.columns if c != feature and c not in filter]  # Extract all other features
    X = data[base_features]  # Collect features for all samples in x
    return X, y


def mod_dframe(df, col='my_col'):
    df = df[df[col].notnull()].copy()
    df[col] = df[col].astype(int).astype(str)

def make_categorical(X, col_labels):
    codes = []
    for col in col_labels:
        X[col], code = pd.factorize(X[col])
        codes.append(code)
    return (X, codes)

def remove_columns(X, col_labels):
    for col in col_labels:
        del X[col]
    return X


def one_hot_encode_categorical(df, col_labels, asBool=False):
    # ONEHOT ENCODING BLOCK
    if asBool:
        df = pd.get_dummies(df, columns=col_labels).astype('bool')
    else:
        df = pd.get_dummies(df, columns=col_labels)
    # X_enc = X_enc.drop(col_labels, axis=1)
    return df
    # mergedata = mergedata.drop(['sex','region','smoker'],axis=1)
    # #END ENCODING BLOCK

def pearson_target(df, target_key='y', threshold=0):
    # Correlation with output variable
    cor = df.corr()
    # sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
    cor_target = abs(cor[target_key])  # Selecting highly correlated features
    relevant_features = cor_target[cor_target > threshold]
    # del relevant_features[target_key]
    return relevant_features


def pearson(df):
    cor = df.corr()
    sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
    #cor_target = abs(cor["MEDV"])
    plt.show()
    #return (cor, cor_target)

def gen_filename(data, opt='auto'):
    import hashlib
    result = hashlib.md5(data.encode())
    from datetime import datetime
    date_time_obj = datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S')
    return "{}_{}_{}".format(date_time_obj,opt, result.hexdigest()[:15])



c_models1={
        'ExtraTreesClassifier': ExtraTreesClassifier(),
        'RandomForestClassifier': RandomForestClassifier(),
        'AdaBoostClassifier': AdaBoostClassifier(),
        'GradientBoostingClassifier': GradientBoostingClassifier(),
        # 'SVC': SVC(),
        'XGBClassifier': XGBClassifier()
    }
c_params1={
        'ExtraTreesClassifier': {'n_estimators': [16, 32]},
        'RandomForestClassifier': {'n_estimators': [16, 32]},
        'AdaBoostClassifier': {'n_estimators': [16, 32]},
        'GradientBoostingClassifier': {'n_estimators': [16, 32], 'learning_rate': [0.8, 1.0]},
        'SVC': [
            {'kernel': ['linear'], 'C': [1, 10]},
            {'kernel': ['rbf'], 'C': [1, 10], 'gamma': [0.001, 0.0001]},
        ],
        'XGBClassifier': {'n_estimators': [16, 32]}
    }


r_models1={
        'LinearRegression': LinearRegression(),
        'Ridge': Ridge(),
        'HuberRegressor': HuberRegressor(),
        'XGBRegressor': XGBRegressor(),
        'SVR': SVR(),
        'SGDRegressor': SGDRegressor(),

    }
r_params1={
        'LinearRegression':{},
        'Ridge': {'alpha': [0.5, 0.75,1]},
        'HuberRegressor': {'alpha': [0.5, 0.75,1], 'epsilon':[1.35,1.5,1]},
        'XGBRegressor': {'n_estimators': [5,20,50,100,250,1000]},
        'SVR': [{'kernel': ['linear'], 'C': [1, 10,50,100]},{'kernel': ['rbf'], 'C': [1, 10,50,100], 'gamma': [0.001, 0.0001]}],
        'SGDRegressor': {'penalty': ['l2', 'elasticnet'], 'learning_rate':['invscaling', 'optimal']},

    }

def mm_gridsearch(X,y,models,params, scoring='r2'):
    helper1 = EstimatorSelectionHelper(models, params)

    helper1.fit(X, y, scoring=scoring, n_jobs=4, refit=True)
    return helper1


def svd(X):
    u, s, v = np.linalg.svd(X, full_matrices=True)
    return (u,s,v)