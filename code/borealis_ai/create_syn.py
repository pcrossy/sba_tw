from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, BaggingRegressor
from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.linear_model import Ridge, Lasso, ElasticNet
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import roc_auc_score, mean_squared_error
from sklearn import preprocessing
from scipy.special import expit
from models import dp_wgan, pate_gan, ron_gauss
from models.Private_PGM import private_pgm
import argparse
import numpy as np
import pandas as pd
import collections
import os
try:
    from models.IMLE import imle
except ImportError as error:
    pass

def gen_syn(model_path, target_variable ='readmitted', opt_name='dpwgan', opt_out='readmission'):
    import pickle
    df_orig_train = pd.read_csv('data/read_train.csv')

    data_columns = [col for col in df_orig_train.columns if col != target_variable]
    fh = open(model_path, 'rb')
    mpategan = pickle.load(fh)
    np_data = mpategan.generate(2000, np.ndarray(2,buffer=np.array([0.45,0.55]), dtype=float))  #samples, class ratios
    X_syn, y_syn = np_data[:, :-1], np_data[:, -1]
    X_syn_df = pd.DataFrame(data=X_syn, columns=data_columns)
    y_syn_df = pd.DataFrame(data=y_syn, columns=[target_variable])
    output_data_path = '{}_{}_synthetic_data.csv'.format(opt_out,opt_name)
    syn_df = pd.concat([X_syn_df, y_syn_df], axis=1)
    syn_df.to_csv(output_data_path, index=False)
    print("Saved synthetic data at : ", output_data_path)


def gen_syn_miri_pate(model_path, target_variable ='charges', opt_name='dpwgan'):
    import pickle
    df_orig_train = pd.read_csv('data/miri_train_nopreprocess.csv')

    data_columns = [col for col in df_orig_train.columns if col != target_variable]
    fh = open(model_path, 'rb')
    mpategan = pickle.load(fh)
    np_data = mpategan.generate(2000, 0.5)  #samples, class ratios
    X_syn, y_syn = np_data[:, :-1], np_data[:, -1]
    X_syn_df = pd.DataFrame(data=X_syn, columns=data_columns)
    y_syn_df = pd.DataFrame(data=y_syn, columns=[target_variable])
    output_data_path = 'miri_{}_synthetic_data.csv'.format(opt_name)
    syn_df = pd.concat([X_syn_df, y_syn_df], axis=1)
    syn_df.to_csv(output_data_path, index=False)
    print("Saved synthetic data at : ", output_data_path)


def get_dataset(filepath, yLabel=None):
    data = pd.read_csv(filepath, encoding="ISO-8859-1")
    y = None
    X = None
    if yLabel is not None:
        y = data[yLabel]  # Get labels
        base_features = [c for c in data.columns if c != yLabel]  # Extract all other features
        X = data[base_features]  # Collect features for all samples in x
        return X, y
    else:
        return data


def miri_continous(df, types=['float', 'float', 'float', 'float', 'float']):
    res = df.copy()
    for i, col_name in enumerate(df.keys()):
        res[col_name] = df[col_name].astype(types[i])
    return res



from datetime import datetime
date_time_obj = datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S')
name = str(input("Choose name for synthetic data set"))
gen_syn('step_10.0_64_20210615_101518.pkl', opt_name='{}_{}'.format(name, date_time_obj), opt_out='read')
#gen_syn_miri_pate('pp_dp-wgan_regression_10.0_20210610_180706', opt_name='{}_{}'.format(name, date_time_obj))