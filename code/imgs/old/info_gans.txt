ctgan_paths = ['models/ctgan_i_plain_wth_target_e4000_b100.pkl',    # 0
                   'models/ctgan_i_plain_wth_target_e10000_b50.pkl',    # 1
                   'models/ctgan_i_plain_wth_target_e10000_b100.pkl',   # 2
                   'models/ctgan_i_plain_wth_target_e10000_b25.pkl',    # 3
                   'models/ctgan_i_plain_wth_target_e3000_b100.pkl',    # 4
                   'models/ctgan_i_plain_wth_target_e1000_b200.pkl',    # 5 Yields best results 0.64
                   'models/ctgan_i_plain_wth_target.pkl']               # 6

Eval Results: [0.6246019689240033, 0.6257039651996595, 0.623649735852016, 0.622296465039272, 0.624137599224134, 0.6236100134296757, 0.6213364738636593]